﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SUApi.Models
{
    public class Catalogs
    {
        [Key]
        public int CatalogId { get; set; }

        public string Day { get; set; }

        public Courses Course { get; set; }

        public int CourseId { get; set; }
    }
}
