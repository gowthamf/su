﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SUApi.Models
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Catalogs> Catalogs { get; set; }

        public DbSet<Courses> Courses { get; set; }

        public DbSet<Lectures> Lectures { get; set; }

        public DbSet<Students> Students { get; set; }

        public DbSet<Users> Users { get; set; }
    }
}
