﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SUApi.Models
{
    public class Lectures
    {
        [Key]
        public int LecturerId { get; set; }

        public string LecturerName { get; set; }

        public Courses Courses { get; set; }

        public int CourseId { get; set; }
    }
}
