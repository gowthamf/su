﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SUApi.Models
{
    public class Students
    {
        [Key]
        public int StudentId { get; set; }

        public string StudentName { get; set; }

        public Courses Courses { get; set; }

        public int CourseId { get; set; }
    }
}
