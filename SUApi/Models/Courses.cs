﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SUApi.Models
{
    public class Courses
    {
        [Key]
        public int CourseId { get; set; }

        public string CourseNames { get; set; }

        public double Fees { get; set; }
    }
}
