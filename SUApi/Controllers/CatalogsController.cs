﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SUApi.Models;

namespace SUApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatalogsController : ControllerBase
    {
        DataContext _dbContext;

        public CatalogsController(DataContext context)
        {
            _dbContext = context;
        }

        [HttpPost("AddCatalog")]
        public IActionResult AddCatalog(Catalogs catalogs)
        {
            _dbContext.Catalogs.Add(catalogs);

            _dbContext.SaveChanges();

            return StatusCode(201, catalogs);
        }
    }
}