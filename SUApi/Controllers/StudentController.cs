﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SUApi.Models;

namespace SUApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        DataContext _dbContext;

        public StudentController(DataContext context)
        {
            _dbContext = context;
        }

        [HttpPost("EnrollCourse")]
        public IActionResult EnrollCourse(Students students)
        {
            _dbContext.Students.Add(students);

            _dbContext.SaveChanges();

            return StatusCode(201,students);
        }
    }
}