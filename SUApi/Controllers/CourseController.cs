﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SUApi.Models;

namespace SUApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        DataContext _dbContext;

        public CourseController(DataContext context)
        {
            _dbContext = context;
        }

        [HttpGet("GetAllCourse")]
        public List<Courses> GetCourses()
        {
            return _dbContext.Courses.ToList();
        }
    }
}