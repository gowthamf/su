﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SUApi.Models;

namespace SUApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        DataContext _dbContext;

        public UsersController(DataContext context)
        {
            _dbContext = context;
        }

        [HttpGet("Login/{userName}/{password}")]
        public Users Login(string userName, string password)
        {
            var user = _dbContext.Users.Where(u => u.UserName == userName && u.Password == password).FirstOrDefault();

            return user;
        }
    }
}