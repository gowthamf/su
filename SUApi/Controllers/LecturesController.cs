﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SUApi.Models;

namespace SUApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LecturesController : ControllerBase
    {
        DataContext _dbContext;

        public LecturesController(DataContext context)
        {
            _dbContext = context;
        }

        [HttpPost("TeachCourse")]
        public IActionResult TeachCourse(Lectures lectures)
        {
            _dbContext.Lectures.Add(lectures);

            _dbContext.SaveChanges();

            return StatusCode(201,lectures);
        }
    }
}