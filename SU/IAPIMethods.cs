﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SU
{
    interface IAPIMethods
    {
        Task<List<Courses>> GetCoursesAsync();

        Task<Lecturer> AddLecturesCourse(Lecturer lecturer);

        Task<bool> AddStudentsEnrollCourse(Student student);

        Task<bool> AddCatalog(Catalogs catalogs);

        Task<Users> LoginUser(string username, string password);
    }
}
