﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SU
{
    public partial class Catalog : Form
    {
        APIClient client = new APIClient();
        public Catalog()
        {
        }

        public Catalog(object courses)
        {
            InitializeComponent();
            var course = (List<Courses>)courses;
            foreach (var item in course)
            {
                comboBox1.Items.Add(item);
            }
            comboBox1.DisplayMember = "CourseNames";

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            var selectedCourse = (Courses)comboBox1.SelectedItem;
            var selectedDay = listBox1.SelectedItem.ToString();
            Catalogs catalogs = new Catalogs{ Day=selectedDay,Course=selectedCourse};
            listBox2.Items.Add(catalogs);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            var selected = listBox2.Items;

            foreach (var item in selected)
            {
                var obj = (Catalogs)item;
                client.AddCatalog(new Catalogs { CourseId=obj.Course.CourseId,Day=obj.Day });
            }
            MessageBox.Show("Catalogs Added Successfully");
        }
    }
}
