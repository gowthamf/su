﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SU
{
    public class APIClient : IAPIMethods
    {
        HttpClient client = new HttpClient();
        public APIClient()
        {
            client.BaseAddress = new Uri("http://localhost:52743/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<bool> AddCatalog(Catalogs catalogs)
        {
            HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/Catalogs/AddCatalog",catalogs);

            if(responseMessage.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<Lecturer> AddLecturesCourse(Lecturer lecturer)
        {
            HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/Lectures/TeachCourse", lecturer);

            if(responseMessage.IsSuccessStatusCode)
            {
                var db= await responseMessage.Content.ReadAsAsync<Lecturer>();
                return await responseMessage.Content.ReadAsAsync<Lecturer>();
            }

            return null;
        }

        public async Task<bool> AddStudentsEnrollCourse(Student student)
        {
            HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/Student/EnrollCourse", student);

            if (responseMessage.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        public async Task<List<Courses>> GetCoursesAsync()
        {
            HttpResponseMessage responseMessage = await client.GetAsync("api/Course/GetAllCourse");

            if(responseMessage.IsSuccessStatusCode)
            {
                return await responseMessage.Content.ReadAsAsync<List<Courses>>();
            }

            return null;
        }

        public async Task<Users> LoginUser(string userName, string password)
        {
            HttpResponseMessage responseMessage = await client.GetAsync($"api/Users/Login/{userName}/{password}");

            if(responseMessage.IsSuccessStatusCode)
            {
                return await responseMessage.Content.ReadAsAsync<Users>();
            }

            return null;
        }
    }
}
