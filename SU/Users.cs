﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SU
{
    public class Users
    {
        public int UserId { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public Lecturer Lecturer { get; set; }

        public Student Student { get; set; }
    }
}
