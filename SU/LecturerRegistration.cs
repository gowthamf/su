﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SU
{
    public partial class LecturerRegistration : Form
    {
        APIClient client = new APIClient();
        
        public LecturerRegistration()
        {
            
        }

        public LecturerRegistration(object course)
        {
            InitializeComponent();

            var courses = (List<Courses>)course;
            foreach (var item in courses)
            {
                comboBox1.Items.Add(item);
            }
            comboBox1.DisplayMember = "CourseNames";
        }

        private async void Button1_Click(object sender, EventArgs e)
        {
            var selectedCourse = (Courses)comboBox1.SelectedItem;
            await client.AddLecturesCourse(new Lecturer { LecturerName = textBox1.Text, CourseId = selectedCourse.CourseId });
            MessageBox.Show("Course Added Successfully");
        }
    }
}
