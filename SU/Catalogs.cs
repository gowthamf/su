﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SU
{
    public class Catalogs
    {
        public int CatalogId { get; set; }
        public Courses Course { get; set; }

        public int CourseId { get; set; }

        public string Day { get; set; }

        public override string ToString()
        {
            return Course.CourseNames + " : " + Day;
        }
    }
}
