﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SU
{
    public class Lecturer
    {
        public int LecturerId { get; set; }

        public string LecturerName { get; set; }

        public Courses Course { get; set; }

        public int CourseId { get; set; }
    }
}
