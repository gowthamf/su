﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SU
{
    public partial class Form1 : Form
    {
        APIClient client = new APIClient();
        public Form1()
        {
            InitializeComponent();
        }

        private async void Button1_Click(object sender, EventArgs e)
        {
            var user=await client.LoginUser(textBox1.Text, textBox2.Text);
            if(user!=null)
            {
                this.Hide();
                DashBoard dashBoard = new DashBoard();
                dashBoard.Show();
            }
            else
            {
                MessageBox.Show("Incorrect User or Password");
            }
        }
    }
}
