﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SU
{
    public class Courses
    {
        public int CourseId { get; set; }

        public string CourseNames { get; set; }

        public double Fees { get; set; }
    }
}
