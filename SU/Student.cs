﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SU
{
    public class Student
    {
        public int StudentId { get; set; }

        public string StudentName { get; set; }

        public Courses course { get; set; }

        public int CourseId { get; set; }
    }
}
