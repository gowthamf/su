﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SU
{
    public partial class DashBoard : Form
    {
        APIClient client = new APIClient();
        public DashBoard()
        {
            InitializeComponent();
        }

        private async void Button2_Click(object sender, EventArgs e)
        {
            var courses = await client.GetCoursesAsync();
            LecturerRegistration lecturerRegistration = new LecturerRegistration(courses);
            lecturerRegistration.ShowDialog();
        }

        private async void Button1_Click(object sender, EventArgs e)
        {
            var courses = await client.GetCoursesAsync();
            StudentRegistration studentRegistration = new StudentRegistration(courses);
            studentRegistration.ShowDialog();
        }

        private async void Button3_Click(object sender, EventArgs e)
        {
            var courses = await client.GetCoursesAsync();
            Catalog catalog = new Catalog(courses);
            catalog.ShowDialog();
        }
    }
}
